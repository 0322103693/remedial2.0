from django.db import models

# Create your models here.

class liga(models.Model):
    codigo = models.CharField(primary_key=True, max_length=50)
    equipo = models.CharField(max_length=50)
    ciudad = models.CharField(max_length=50)
    estadio = models.CharField(max_length=50)
    
    def __str__(self):
        texto = "{0}, {1}, {2}"
        return texto.format(self.equipo, self.ciudad, self.estadio)
    
