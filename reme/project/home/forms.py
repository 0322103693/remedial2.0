# En forms.py en tu aplicación
from django import forms
from .models import NflModel

class EditarDatosForm(forms.ModelForm):
    class Meta:
        model = NflModel
        fields = ['codigo', 'equipo', 'ciudad', 'estadio']
        widgets = {
            'codigo': forms.TextInput(attrs={'readonly': 'readonly', 'class': 'form-control'}),
            'equipo': forms.TextInput(attrs={'class': 'form-control'}),
            'ciudad': forms.TextInput(attrs={'class': 'form-control'}),
            'estadio': forms.TextInput(attrs={'class': 'form-control'}),
        }
